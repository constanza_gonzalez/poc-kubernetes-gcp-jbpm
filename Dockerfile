#FROM apiuxcl/dev-docux-jbpm-final
FROM apiuxcl/clean-docux-jbpm:v1

ENV IP_APACHEDS=10.31.0.2
ENV IP_POSTGRES=10.31.0.2
ENV POSTGRES_PASSWORD=FWyQ#t2J%rMw+5M

RUN rm -rf /opt/wildfly-8.2.1.Final/standalone/configuration/standalone_xml_history

CMD ["./opt/wildfly-8.2.1.Final/bin/standalone.sh", "--server-config=standalone-full.xml", "-Dfile.encoding=UTF-8"]
